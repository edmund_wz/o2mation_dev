# Stubs for helper (Python 3)
#
# NOTE: This dynamically typed stub was automatically generated by stubgen.

from typing import Any

class Helper:
    def __init__(self) -> None: ...
    def uuid(self): ...
    def md5(self, key: Any, count: int = ...): ...
    def encode(self, machineId: Any, expired: Any, currentDate: Any): ...
    def decode(self, key: Any): ...
