import control
class ExamplePoint(ControlPoint):
    def __init__(self):
        super().__init__()
        self.set_icon('icon-number')
        self.out = self.new_property("s", 0)

    def set_out(self, v):
        self.set_value(self.out, v)